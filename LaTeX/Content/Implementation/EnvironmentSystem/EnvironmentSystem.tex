Um die Anforderungen aus Kategorie 3 zu erfüllen, ist es notwendig, eine 
Sonnen- und Mondstands-Berechnung für die jeweilige Zeit und den Ort 
durchzuführen. Dies soll im Folgenden dargestellt werden. Zu diesem 
Themenbereich seien folgende weiterführende Quellen empfohlen: 
\cite{MondPosition1}, \cite{WikiMittelpunktsgleichung}, \cite{WikiMondbahn}, 
\cite{WikiSonnenstand}, \cite{WikiSternzeit} und \cite{MondPosition}.

\subsection{Berechnung der Zeit}
Zu diesem Zweck muss zunächst die koordinierte Weltzeit in eine für 
astronomische Berechnungen übliche Zeit überführt werden. Mit dieser Zeit 
lässt sich dann die Position von Sonne und Mond in Horizontalkoordinaten 
darstellen.

\medskip
Dieses Koordinatensystem stellt die Position eines Himmelskörpers durch seine 
Höhe über dem Horizont ($h$) in Grad und seinen Winkel zur Nord-Süd-Achse 
ebenfalls in Grad, dem Azimut ($a$), dar
\QuoteIndirect{SterneImComputer}{S. 193}. 
\index{Koordinaten!Horizontal}
\index{Koordinaten!Horizontal!Höhe}
\index{Koordinaten!Horizontal!Azimut}

\subsubsection{Berechnung des Julianischen Datums} \label{subsubsec:JD}
Zunächst muss die koordinierte Weltzeit in das Julianische Datum überführt 
werden. Dieses Datum zählt die Tage fortlaufend seit dem 1. Januar -4712 um 
12:00 in \ac{GMT}.

\medskip
Da ein tropisches Jahr eine Länge von 365,242198 mittleren Sonnentagen hat, 
aber ein Kalenderjahr nur aus 365 Tagen besteht, kommt es zu einer 
Abweichung, die durch die Schaltjahre ausgeglichen wird.

Ab 46 vor Christus, durch Gaius Julius Cäsar eingeführt, wurde jedes vierte 
Jahr auf 366 Tage erweitert, dadurch hatte ein Jahr durchschnittlich eine Länge 
von 365,25 Tagen. Die Abweichung gegenüber dem tropischen Jahr von ca. 11 
Minuten summiert sich in 130 Jahren zu einem Tag.

Bis zum 16. Jahrhundert ist so eine wahrnehmbare Abweichung entstanden. Um 
diese Abweichung auszugleichen, wurden einige Tage verworfen, deswegen folgt 
auf den 4. Oktober 1582 direkt der 10. Oktober 1582 
\QuoteIndirect{SterneImComputer}{S. 93 f.}. Somit ergeben sich drei mögliche 
Fälle: \index{Jahr!Tropisch} \index{Jahr!Schaltjahr}

\begin{enumerate}
	\setlength\itemsep{\Itemizespace}
	\item \label{enum:old}Bürgerliches Kalenderdatum liegt vor dem (oder ist 
		  der) 4. Oktober 1582.
	\item \label{enum:noJD} Bürgerliches Kalenderdatum liegt zwischen dem 4. 
		  und 15. Oktober 1582.
	\item \label{enum:today}Bürgerliches Kalenderdatum liegt nach dem (oder 
		  ist der) 15. Oktober 1582.
\end{enumerate}

Im Fall \ref{enum:noJD} kann kein Julianisches Datum berechnet werden. Somit 
bleiben noch Fall \ref{enum:old} und \ref{enum:today} übrig. In 
\autoref{eq:JDdiff} werden diese Fälle unterschieden 
\QuoteIndirect{AstronomicalAlgorithms}{S. 60 f.}. 

\begin{equation} \label{eq:JDdiff}
A =
\begin{dcases*}
	0 & 
		für Fall \ref{enum:old} \\
	\text{Int}\left(\frac{J}{100}\right) &
		für Fall \ref{enum:today}
\end{dcases*}
\: \text{und} \:
B = 
\begin{dcases*}
	0 & 
		für Fall \ref{enum:old} \\
	2 - A + \text{Int}\left(\frac{A}{4}\right) &
		für Fall \ref{enum:today}
\end{dcases*}
\end{equation}

Die Definition von $J$ und $M$ in \autoref{eq:JDMonth} ergibt sich aus dem 
Umstand, dass der März bei dieser Berechnung der erste Monat im Jahr ist und 
somit der letzte Tag im Jahr zum Schaltjahr wird, wobei die Monate von 1--12 
durchnummeriert werden
\QuoteIndirect{SterneImComputer, AstronomicalAlgorithms}{S. 94 f. und S. 61}. 

\begin{equation} \label{eq:JDMonth}
J =
\begin{dcases*}
	\text{Jahr} & 
		wenn Monat > 2 \\
	\text{Jahr} - 1 & 
		sonst
\end{dcases*}
\: \text{und} \:
M = 
\begin{dcases*}
	\text{Monat} &
		wenn Monat > 2 \\
	\text{Monat} + 12 &
		sonst
\end{dcases*}
\end{equation}

Das Julianische Datum ist in den obigen Quellen nur für ganze Tage definiert, 
dies ist für die Berechnung des Sonnen- und Mondstandes zu ungenau. Jedoch 
können die Stunden, Minuten und Sekunden, wie \autoref{eq:JDD} zeigt, als 
Tagesbruchteil ergänzt werden. Wobei $T$ die Tage und $H$ die Stunden 
darstellt. \index{Tagesbruchteil}

\begin{equation} \label{eq:JDD}
H = \frac{\text{Stunden}}{24} + \frac{\text{Minuten}}{1\,440} +
	\frac{\text{Sekunden}}{86\,400}
\end{equation}

Das Julianische Datum ($JD$) berechnet sich nun wie in \autoref{eq:JD} 
dargestellt. Wobei $\text{Int}(x)$ eine Funktion ist, die die Vorkommastellen 
von $x$ zurückgibt \QuoteIndirect{AstronomicalAlgorithms}{S. 61, Gl. 7.1}.

\begin{equation} \label{eq:JD}
JD = \text{Int}(356,25 \cdot (J + 4\,716)) + \text{Int}(30,600\,1 \cdot (M + 
1)) + T + H + B - 1\,524,5
\end{equation}

Leider ergibt sich bei der Implementierung dieser Gleichungen in die \ac{UE4}
ein Problem:

Die \ac{UE4} rundet Fließkommazahlen nach einer Mantissenlänge von ca. 5--6 
Stellen. Für die meisten Berechnungen ist dies ausreichend, aber für die 
Berechnung des Julianischen Datums reicht das nicht. Dadurch wird 
2457332,044398 (05.11.2015 14:03:56) zu 2457332,0 und 2457332,421690 
(05.11.2015 23:07:14) zu 2457332,5. Somit springen die Nachkommastellen 
viermal am virtuellen Tag von 0,0 auf 0,25 0,75 1,0. Dadurch springt auch der 
Mond (sichtbar) und die Sonne (minimal) auf eine andere Position. Die Vor- und 
Nachkomma-Werte getrennt voneinander zu behandeln, führt zu keiner 
Verbesserung, da diese Werte vor der nächsten Berechnungsstufe wieder gerundet 
werden.

\subsubsection{Berechnung der Sternzeit} \label{subsubsec:SediralTime}
Die Sternzeit ist eine in der Astronomie gebräuchliche Zeit, bei der ein 
Sterntag die Zeit zwischen zwei aufeinanderfolgenden Meridiandurchgängen des
Frühlingspunktes ist. Da sich die Erde in der Zeit zwischen diesen 
Durchgängen um die Sonne bewegt, unterschiedet sich ein Sternzeit-Tag von 
einem Sonnenzeit-Tag um ca. vier Minuten \QuoteIndirect{SterneImComputer}
{S. 117}.\index{Sternzeit}

Der Meridian stellt eine gedachte Linie zwischen Norden und Süden dar
\QuoteIndirect{SterneImComputer}{S. 193}. Der Frühlingspunkt lässt sich durch 
folgenden Satz beschreiben: \QuoteDirect{Legt man durch den Erdäquator eine 
Ebene, so schneidet diese die Erdbahnebene in zwei Punkten, dem Frühlings- 
und dem Herbstpunkt}{SterneImComputer}{S. 29}. \index{Frühlingspunkt}
\index{Meridian}

\medskip
Zunächst wird, wie in \autoref{eq:STT0} und \autoref{eq:STThetha0} gezeigt, 
die Sternzeit am Greenwich Meridian um Mitternacht ($\Theta_0 $) in Grad 
berechnet \QuoteIndirect{AstronomicalAlgorithms}{S. 87 f., Gl. 12.1}.

\begin{equation} \label{eq:STT0}
T = \frac{JD_0 - 2\,451\,545,0}{36\,525,0}
\end{equation}

Hierbei ist $JD_0$ das Julianische Datum für den entsprechenden Tag um 
Mitternacht. Da das Julianische Datum die Tage ab 12:00 mittags zählt, 
muss das Julianische Datum immer auf 0,5 enden
\QuoteIndirect{AstronomicalAlgorithms}{S. 87 f., Gl. 12.3}.

\begin{equation} \label{eq:STThetha0}
\Theta_0 = 100,460\,618\,37 + 36\,000,770\,053\,608 \cdot T + 0,000\,387\,933 
\cdot T^2 - \frac{T^3}{38\,710\,000}
\end{equation}

Um aus diesem Wert nun die Sternzeit für eine beliebige \ac{UT}-Zeit 
($\theta_0$) zu berechnen, bietet sich \autoref{eq:STt0} an
\QuoteIndirect{AstronomicalAlgorithms}{S. 87 f.}.

\begin{equation} \label{eq:STt0}
\theta_0 = UT \cdot 1,002\,737\,909\,35 + \Theta_0
\end{equation}

Nun kann die Nutation \QuoteIndirect{AstronomicalAlgorithms}{S. 143 ff.},
eine periodische Pendelbewegung, der Erdachse, die durch die Mondanziehung  
entsteht, noch mit \autoref{eq:nut} eingerechnet werden, wobei $\Delta \psi$ 
die Nutation des Längengrades und $\varepsilon$ die Nutation des 
Breitengrades darstellt. Dieser Term wird auf $\theta_0$ addiert
\QuoteIndirect{AstronomicalAlgorithms}{S. 87 f.}. \index{Nutation}

\begin{equation} \label{eq:nut}
\frac{\Delta \psi \cdot \cos(\varepsilon)}{15}
\end{equation}

Abschließend muss die Sternzeit am Greenwich Meridian noch umgerechnet werden 
in die Sternzeit am Beobachtungspunkt, dies geschieht mittels 
\autoref{eq:LST}. $\lambda$ ist der Längengrad des Beobachtungspunktes.

\begin{equation} \label{eq:LST}
\theta_\lambda = \theta_0 + \lambda
\end{equation}

Alternativ soll ein einfacheres und ungenaueres Verfahren dargestellt werden, 
bestehend aus \autoref{eq:STTHG} und \ref{eq:STTG}.

\begin{equation} \label{eq:STTHG}
\theta^h_G = 6,697\,376 + 2\,400,051\,34 \cdot T + 1,002\,738 \cdot UT
\end{equation}

Dabei stellt der erste Term in \autoref{eq:STTHG} die Greenwich Sternzeit um 
J2000.0 dar, das tägliche Vorrücken von ca. vier Minuten wird im zweiten Term 
dargestellt und der dritte Term addiert die aktuelle Tageszeit als Bruchteil,
in Stunden mit Minuten und Sekunden als Nachkommastellen 
(15:30 $\rightarrow$ 15,50).

\begin{equation} \label{eq:STTG}
\theta_G = \theta^h_G \cdot 15
\end{equation}

$\theta_G$ aus \autoref{eq:STTG} kann nun anstelle von $\theta_0$ in 
\autoref{eq:LST} benutzt werden \QuoteIndirectNoPage{WikiSonnenstand}.

\subsection{Berechnung des Sonnenstandes}
Die Genauigkeit der Berechnung 
\QuoteIndirectNoPage{LowPrecisionFormulaeforPlanetaryPositions} ist 
ausreichend, da der durch die fehlende Mantissenlänge der \ac{UE4}-Berechnungen 
erzeugte Fehler größer ist als der Fehler in der Berechnung des 
Sonnenstandes.

\medskip
In der Astronomie ist es üblich, eine Stan"-dard"-epoche zu definieren. Eine 
Stan"-dard"-epoche ist in diesem Kontext ein Zeitpunkt, zu dem Werte (z.B. die 
Position) von Himmelskörpern genau bestimmt werden, sodass sich später die 
Positionen der Himmelskörper aus dem Wert zu dieser Standardepoche plus die 
dazu gekommene Abweichung berechnen lassen.

Die aktuelle Standardepoche ist J2000.0, der 1.1.2000 um 12:00 mittags. So 
lässt sich $n$ als Differenz in Tagen zwischen J2000.0 und der aktuellen 
Beobachtungszeit definieren. \autoref{eq:SunnN} stellt dies dar, wobei $JD$
das Julianischen Datum ist \SeeS{subsubsec:JD}. Dabei ist zu beachten, dass in 
einer Quelle eine andere Standardepoche und damit auch eine andere Zahl 
(2433282,5) benutzt wird \QuoteIndirect{AstronomicalAlgorithms}{S. 293}.
In dieser Quelle ist die richtige Epoche zu finden 
\QuoteIndirect{SterneImComputer}{S. 171, Gl. 3.6.3}.
\index{Standardepoche}

\begin{equation} \label{eq:SunnN}
n = JD - 2\,451\,545,0
\end{equation}

\subsubsection{Berechnung des Sonnenstandes in Ekliptikalkoordinaten}
Zunächst wird angenommen, dass sich die Erde auf einer Kreisbahn um die Sonne 
bewegt, so lässt sich eine \emph{mittlere} ekliptikale Länge ($L$) der Sonne 
aus \autoref{eq:SunnL} ermitteln \QuoteIndirect{SterneImComputer}
{S. 171 Gl. 3.6.4}. Dabei setzt sich die Sonnen-Geschwindigkeit wie in 
\autoref{eq:SunnSpeed} gezeigt zusammen.

\begin{equation} \label{eq:SunnSpeed}
\frac{365,242\,198 \text{ Tage}}{360^\circ} = 0,985\,647\,4^\circ / Tag
\end{equation}

Das Ekliptikale Koordinatensystem stellt die Position des zu betrachtenden 
Himmelskörpers (in diesem Fall der Sonne) in Abhängigkeit von der Ekliptik 
der Erde dar. So misst die ekliptikale Länge ($\Lambda$) die Wanderung der 
Sonne vom Frühlingspunkt aus auf der Ekliptik in Grad; die ekliptikale 
Breite ($\beta$) stellt die Abweichung des zu betrachtenden Himmelskörpers von 
dieser Ebene in Grad dar (positiv nach Norden). Da die Sonne betrachtet wird 
gibt es keine Breite, denn definitionsgemäß liegt die Sonne immer auf der 
Ekliptik.
\index{Koordinaten!Ekliptikal}
\index{Koordinaten!Ekliptikal!Länge}
\index{Koordinaten!Ekliptikal!Breite}

\begin{equation} \label{eq:SunnL}
L = 280,460^\circ + 0,985\,647\,4^\circ / Tag \cdot n
\end{equation}

Diese Annahme kann nun präzisiert werden, indem der Einfluss der realen, 
ellipsenförmigen Erdbahn durch die Mittelpunktsgleichung ergänzt wird, so 
lässt sich die ekliptikale Länge ($\Lambda$) berechnen. Dazu muss zunächst die 
mittlere Anomalie ($g$) (Winkel zwischen der Sonne und dem sonnennächsten 
Punkt auf der Erdumlaufbahn) berechnet werden, siehe \autoref{eq:Sunng} 
\QuoteIndirect{SterneImComputer}{S. 171 Gl. 3.6.5}. 
\index{Mittelpunktsgleichung} \index{Anomalie!mittlere}

\begin{equation} \label{eq:Sunng}
g = 357,528^\circ + 0,985\,600\,3^\circ \cdot n
\end{equation}

Die Mittelpunktsgleichung ist periodisch und kann deswegen wie in 
\autoref{eq:SunnLambda} als Reihe dargestellt werden, dabei ist die 
Exzentrizität $e \approx 0,0167$ schon eingesetzt
\QuoteIndirectNoPage{Mittelpunktsgleichung}.

\begin{equation} \label{eq:SunnLambda}
\Lambda - L = \left( 2e \sin(g) + \frac{5}{4}e^2 \sin(2g) + \dots \right) 
\cdot \frac{180^\circ}{\pi}
\end{equation}

Für geringe Bahnexzentrizitäten lässt sich diese Reihe nach wenigen Gliedern 
abbrechen. Nach $\Lambda$ umgestellt ergibt sich \autoref{eq:SunnLambda2} 
\QuoteIndirectNoPage{Mittelpunktsgleichung}.

\begin{equation} \label{eq:SunnLambda2}
\Lambda = L + 1,915^\circ \cdot \sin(g) + 0,019\,97^\circ \cdot \sin(2g)
\end{equation}

\subsubsection{Berechnung des Sonnenstandes in Äquatorialkoordinaten}
Zunächst soll kurz das (rotierende) äquatoriale Koordinatensystem 
beschrieben werden.

Dabei wird der Himmel als eine die Erde umhüllende Kugel betrachtet, diese 
Kugel besitzt, wie auch die Erde, einen Äquator. Dieser Himmelsäquator bildet 
die Basis des äquatoriale Koordinatensystems. Die Rektaszension ($\alpha$) 
beschreibt den Winkel zwischen dem Frühlingspunkt und der geographischen 
Länge des Beobachtungsortes. Die Deklination ($\delta$) beschreibt den Winkel 
zwischen dem Himmelsäquator und dem betrachteten Himmelskörper, vergleichbar 
mit dem Breitengrad auf der Erde.
\index{Koordinaten!Äquatorial}
\index{Koordinaten!Äquatorial!Deklination}
\index{Koordinaten!Äquatorial!Rektaszension}

\begin{equation} \label{eq:SunnVarepsilon}
\varepsilon = 23,439^\circ - 0,000\,000\,4^\circ \cdot n
\end{equation}

Der Winkel der Ekliptik ($\varepsilon$) muss für die Berechnung des 
Sonnenstandes bekannt sein. \autoref{eq:SunnVarepsilon} zeigt seine 
Berechnung. Dabei stellen die $23,439^\circ$ die Erdneigung und 
$0,000\,000\,4^\circ \cdot n$ die Änderung pro Tag dar
\QuoteIndirect{AstronomicalAlmanach}{S. C24}.

\begin{equation} \label{eq:SunnAlpha}
%\alpha = \arctan \left( \frac{\cos(\varepsilon) \cdot \sin(\Lambda)}
%{\cos(\Lambda)} \right)
\alpha = \arctan \left( \cos(\varepsilon) \cdot \tan(\Lambda) \right)
\end{equation}

Die Rektaszension ($\alpha$) ergibt sich aus \autoref{eq:SunnAlpha}. Dabei ist 
zu beachten, dass $\alpha$ und $\Lambda$ im gleichen Quadranten liegen müssen, 
siehe \autoref{eq:SunnLamKor} \QuoteIndirect{AstronomicalAlmanach}{S. C24}.

\begin{equation} \label{eq:SunnLamKor}
\alpha =
	\begin{dcases}
		\alpha + 180^\circ & 
			\text{wenn} \cos(\Lambda) < 0 \\
		\alpha & 
			\text{sonnst}
	\end{dcases}
\end{equation}

Die Deklination ($\delta$) ergibt sich nun aus \autoref{eq:SunnDelta}
\QuoteIndirect{AstronomicalAlmanach}{S. C24}.

\begin{equation} \label{eq:SunnDelta}
\delta = \arcsin \left( \sin(\varepsilon) \cdot \sin(\Lambda) \right)
\end{equation}

\subsubsection{Berechnung des Sonnenstandes in Horizontalkoordinaten}
Nun wird die Sternzeit aus \autoref{subsubsec:SediralTime} benutzt, um den
Sonnenstand in Horizontalkoordinaten zu überführen 
\QuoteIndirect{AstronomicalAlgorithms}{S. 92 ff., Gl. 13.5 und 13.6}.

\begin{equation} \label{eq:SunnTau}
\tau = \theta_\lambda -\alpha
\end{equation}

Dazu wird der Stundenwinkel ($\tau$) in \autoref{eq:SunnTau} berechnet
\QuoteIndirect{SterneImComputer}{S. 194, Gl. 3.8.7}.

\begin{equation} \label{eq:SunnA}
a = \arctan \left( \frac{\sin(\tau)}{\cos(\tau) \cdot \sin(\varphi) - 
\tan(\delta) \cdot \cos(\varphi)} \right)
\end{equation}

Anschließend ergeben sich Azimut ($a$) und Höhenwinkel ($h$) aus 
\autoref{eq:SunnA} und \autoref{eq:SunnH}. Hierbei muss 
\autoref{eq:SunnAKor} beachtet werden.

\begin{equation} \label{eq:SunnH}
h = \arcsin \left( \cos(\delta) \cdot \cos(\tau) \cdot \cos(\varphi) + 
\sin(\delta) \cdot \sin(\varphi) \right)
\end{equation}

Hierbei stellt $\varphi$ den Breitengrad des Beobachtungsortes dar. 

\begin{equation} \label{eq:SunnAKor}
a =
	\begin{dcases}
		a + 180^\circ & 
			\text{wenn} \left( \frac{\sin(\tau)}{\cos(\tau) \cdot 
			\sin(\varphi) - \tan(\delta) \cdot \cos(\varphi)} \right) < 0 \\
		a & 
			\text{sonnst}
	\end{dcases}
\end{equation}

\subsubsection{Höhenkorrektur wegen Refraktion} \label{subsubsec:sunRef}
Der Sonnenstand liegt nun in den geforderten Koordinaten vor, jedoch 
entspricht er noch nicht dem visuellen Eindruck auf der Erde, da sich durch 
die Lichtbrechung in der Erdatmosphäre die scheinbare Position der Sonne 
nach oben verzerrt. Im Zenit ist die Abweichung null, nimmt zum Horizont zu 
und ist am Horizont so stark, dass die Sonne, obwohl sie schon untergegangen 
ist, immer noch am Horizont gesehen werden kann. \index{Refraktion}

Leider lässt sich diese Störung ($R$) nicht genau berechnen, da sie von den 
Bedingungen in der Atmosphäre abhängt, wie z.B. Luftdruck und Temperatur. 
Auch die Wellenlänge des Lichtes übt einen Einfluss auf die Refraktion aus. 
\autoref{eq:SunnR} gibt  eine Näherung der Refraktionsabweichung in 
Bogenminuten für 1010 hPa und 10 $^\circ$C an 
\QuoteIndirect{AstronomicalAlgorithms}{S. 105 ff., Gl. 16.3}.

\begin{equation} \label{eq:SunnR}
R = \frac{1,02}{\tan\left(h+\frac{10,3}{h+5,11}\right)}
\end{equation}

\autoref{eq:SunnHR} zeigt, wie dieser Wert auf $h$ addiert wird.

\begin{equation} \label{eq:SunnHR}
h_R = h + \frac{R}{60}
\end{equation}

\subsection{Berechnung des Mondstandes \& Phase}
Die Berechnung des Mondstandes ist anders aufgebaut als die der Sonne. Der 
Mond bewegt sich auf einer elliptischen Keplerbahn um die Erde. Die Berechnung 
dieser Bahn ist zwar einfach möglich, aber nicht sonderlich genau, da viele 
Störungen einen Einfluss auf die Position des Mondes haben. So ziehen sich 
Mond und Erde gegenseitig an, auch die anderen Planeten und die Sonne haben 
einen merklichen Einfluss auf die Position des Mondes. Aus diesem Grund werden 
die Einflüsse auf die elliptische Keplerbahn durch eine Reihenentwicklung von 
Korrekturfaktoren beschreiben.

Astronomische Recheninstitute bringen bis zu 1500 Korrekturen an 
\QuoteIndirect{SterneImComputer}{S. 235 ff.}. Da die \ac{UE4} sowieso nicht 
sehr genau rechnet (siehe \autoref{subsubsec:JD}) sind deutlich weniger 
Korrekturen ausreichend. Dargestellt werden 31 Korrekturen, selbst das ist 
noch zu viel.

\subsubsection{Berechnung des Mond- und Sonnenlängengrades in der Ekliptik}
Um die Mondphase zu berechnen, ist es notwendig, die ekliptikale Länge der 
Sonne und des Mondes zu bestimmen. Die ekliptikale Länge des Mondes lässt sich 
aus \autoref{eq:MoonLength} berechnen \QuoteIndirect{SterneImComputer}
{S. 170 Gl. 3.6.1}, wobei sich die Berechnung von $T$ in \autoref{eq:STT0} 
findet.

\begin{equation} \label{eq:MoonLength}
\begin{aligned}
L_M & = 218,32 \\ 
	& \quad + 481\,267,883 \cdot T \\
	& \quad + 6,29       \cdot \sin(134,9 + 477\,198,85  \cdot T) \\
	& \quad - 1,27       \cdot \sin(259,2 - 413\,335,38  \cdot T) \\
	& \quad + 0,66       \cdot \sin(235,7 + 890\,534,23  \cdot T) \\
	& \quad + 0,21       \cdot \sin(269,9 + 954\,397,7   \cdot T) \\
	& \quad - 0,19       \cdot \sin(357,5 +  35\,999,05  \cdot T) \\
	& \quad - 0,11       \cdot \sin(186,6 + 966\,404,05  \cdot T) \\
\end{aligned}
\end{equation}

Dabei ist zu beachten, dass $L_M$ in Grad gemessen wird, somit ist es sinnvoll 
diesen Wert durch $L_M \mod{360}$ in den Bereich zwischen $0^\circ$ und 
$360^\circ$ zu bringen.

\begin{equation} \label{eq:SunnLengthS}
L_S = L + 1,915 \cdot \sin(G) + 0,02 \sin(2 \cdot G)
\end{equation}

Die ekliptikale Länge der Sonne ($L_S$) berechnet sich nun aus 
\autoref{eq:SunnLengthS} \QuoteIndirect{SterneImComputer}{S. 170 Gl. 3.6.6}, 
wobei sich die Berechnungen von $L$ und $G$ in \autoref{eq:SunnL} und 
\ref{eq:Sunng} finden. Auch $L_S$ wird in Grad gemessen und ist in den Bereich 
zwischen $0^\circ$ und $360^\circ$ zu bringen.

\subsubsection{Berechnung des Mondalters und der Sichtlänge}
\label{subsubsec:moonage}
Das Mondalter ist ein Wert zwischen 0 und 29,5 und gibt die Tage nach Neumond 
an, er berechnet sich aus \autoref{eq:MoonAge} 
\QuoteIndirect{SterneImComputer}{S. 171, Gl. 3.6.7 und S. 173}.
\index{Mondalter}

\begin{equation} \label{eq:MoonAge}
\text{Mondalter} = \frac{L_M - L_S}{12,191}
\end{equation}

Die Lage des Terminator ($T_M$) wird in \autoref{eq:lengthOfSight} berechnet.
Der Terminator ist die Grenze zwischen der erleuchteten und dunklen Mondseite, 
wobei $0^\circ$ dem westlichen Rand und $180^\circ$ dem östlichen Rand 
entsprechen, $90^\circ$ entspricht somit genau Halbmond
\QuoteIndirect{SterneImComputer}{S. 171, Gl. 3.6.8}.
\index{Terminator}

\begin{equation} \label{eq:lengthOfSight}
T_M = L_M - L_S
\end{equation}

\subsubsection{Berechnung der Mondposition}
Wie oben beschrieben, besteht die Berechnung der Mondposition zum 
überwiegenden Teil aus einer Reihe von Störkoeffizienten. Die hier 
dargestellte Berechnung unterscheidet sich von der implementierten 
Berechnung, sie wurde vereinfacht, so finden sich in der Implementierung 50 
Störkoeffizienten und die Zahlen sind genauer aufgelöst. Das dahinter 
stehende Prinzip bleibt gleich und die hier nicht beschriebenen Werte finden 
sich z.B. in \cite[S. 337 ff. und Tab. 47a/47b]{AstronomicalAlgorithms}.

\begin{alignat}{5}
& L_0 &&  = 270,43 && + 481\,267,883\,1 \cdot T && - 0,001\,133 \cdot T^2 && + 0,000\,001\,9 \cdot T^3 \label{eq:MoonL0} \\
& M_0' && = 296,1 &&  + 477\,198,849\,1 \cdot T && + 0,009\,192 \cdot T^2 && + 0,000\,014\,4 \cdot T^3 \label{eq:MoonM0} \\
& k &&    = 259,18 && - 1\,934,142 \cdot T &&      + 0,002\,078 \cdot T^2 && + 0,000\,002\,2 \cdot T^3 \label{eq:Moonk} \\
& e &&    = 1 &&      - 0,002\,495 \cdot T &&      - 0,000\,007\,52 \cdot T^2 && \label{eq:Moone} \\
%& N &&    = 275,05 && - 2,3 \cdot T && && \label{eq:MoonN} \\
& T_1 &&  = 51,2 &&   + 20,2 \cdot T && && \label{eq:MoonT1} \\
& T_2 &&  = 346,56 && + 132,87 \cdot T &&          - 0,009\,173 \cdot T^2  && \label{eq:MoonT2}
\end{alignat}

Zunächst müssen einige Hilfsgrößen bestimmt werden: unter anderem die 
mittlere Länge des aufsteigenden Knotens ($k$), Exzentrizität der Erdbahn 
($e$) und zwei Störungsargumente ($T_1$ und $T_2$). Dies wird in 
\autoref{eq:MoonL0} bis \ref{eq:MoonT2} gezeigt
\QuoteIndirect{SterneImComputer}{S. 236, Tab. 4.2-1 und Gl. 4.1.1}.
Dabei gibt die Exzentrizität die Abweichung von der perfekten Kreisbahn an.
Die Länge des aufsteigenden Knotens ist der Winkel zwischen dem 
Frühlingspunkt und dem aufsteigenden Knoten. Hierbei ist der aufsteigende 
Knoten der Punkt im Raum, an dem sich die Bahn des Mondes und die Ekliptikebene 
der Erde schneiden.
\index{Länge des aufsteigenden Knotens}
\index{Exzentrizität}

\begin{alignat}{5}
%& D_0 && = L_0 && - L_S && && \label{eq:MoonD0} \\
%& F_0 && = L_0 && - k && && \label{eq:MoonF0} \\
& L   && = L_0 && + 0,004 \cdot \sin(T_2) && + 0,002 \cdot \sin(k) && \label{eq:MoonL} \\
%& D   && = D_0 && +  0,002 \cdot \sin(T_1)&&  + 0,004 \cdot \sin(T_2) && + 0,002 \cdot \sin(k) \label{eq:MoonD} \\
%& F   && = F_0 && + 0,004 \cdot \sin(T_2) && - 0,25 \cdot \sin(k) && - 0,004 \cdot \sin(N + k) \label{eq:MoonF} \\
%& M   && = M_S && - 0,002 \cdot \sin(T_1) && && \label{eq:MoonM} \\
& M'  && = M_0' && + 0,001 \cdot \sin(T_1) && + 0,004 \cdot \sin(T_2) && + 0,003 \cdot \sin(k) \label{eq:MoonM'} \\
& f   && = 6,29 \cdot \sin(M') && + 0,21 \cdot \sin(2 \cdot M') && + 0,01 \cdot \sin(3 \cdot M') \label{eq:Moonf}
\end{alignat}

Mithilfe dieser Werte lassen sich nun die mittlere Länge des Mondes ($L$), 
die mittlere Anomalie des Mondes ($M'$) und die Mittelpunktsgleichung ($f$) 
aus \autoref{eq:MoonL} bis \ref{eq:Moonf} berechnen,
\QuoteIndirect{SterneImComputer}{S. 236, Gl. 4.2.3, 4.2.7 und 4.2.8}.

\begin{align}
L_n & = C_n \cdot \sin \left(i \cdot D + j \cdot F + k \cdot  M + l \cdot M' \right) \label{eq:MoonLn} \\
B_n & = C_n \cdot \sin \left(i \cdot D + j \cdot F + k \cdot  M + l \cdot M' \right) \label{eq:MoonBn}
\end{align}

Nun müssen die Störungen berechnet werden, dies geschieht mittels 
\autoref{eq:MoonLn} und \ref{eq:MoonBn}. Die Werte dazu finden sich in \autoref{tab:Moonl} und \ref{tab:Moonb}
\QuoteIndirect{SterneImComputer}{S. 236, Gl. 4.2.10 und 4.2.11}.
So berechnet sich $L_1$ zu $1,274 \cdot \sin(2 \cdot D - M')$.

\begin{table}[htb]
\begin{minipage}{.5\linewidth}
\centering
\begin{tabular}{|c|r<{,\kern-\tabcolsep}>{\kern-\tabcolsep}l|c|c|c|c|}
	\hline
		$L_N$ & \multicolumn{2}{c|}{$C_N$} & $i$ & $j$ & $k$ & $l$ \\
	\hline
	\EmptyRow
	\hline 
		$L_1$ & 1 & 274 & 2 & 0 & 0 & -1 \\ 
	\hline \Grayrow
		$L_2$ & 0 & 658 & 2 & 0 & 0 & 0 \\ 
	\hline
		$L_3$ & -0 & $186 \cdot e$ & 0 & 0 & 1 & 0 \\ 
	\hline \Grayrow
		$L_4$ & -0 & 114 & 0 & 2 & 0 & 0 \\ 
	\hline
		$L_5$ & -0 & 059 & -2 & 0 & 0 & 2 \\ 
	\hline \Grayrow
		$L_6$ & 0 & $057 \cdot e$ & 2 & 0 & -1 & -1 \\ 
	\hline
		$L_7$ & 0 & 053 & 2 & 0 & 0 & 1 \\ 
	\hline \Grayrow
		$L_8$ & 0 & $046 \cdot e$ & 2 & 0 & -1 & 0 \\ 
	\hline
		$L_9$ & 0 & $041 \cdot e$ & 0 & 0 & -1 & 1 \\ 
	\hline \Grayrow
		$L_{10}$ & -0 & 035 & 1 & 0 & 0 & 0 \\ 
	\hline
		$L_{11}$ & -0 & $030 \cdot e$ & 0 & 0 & 1 & 1 \\ 
	\hline \Grayrow
		$L_{12}$ & -0 & 015 & -2 & 2 & 0 & 0 \\ 
	\hline
		$L_{13}$ & -0 & 013 & 0 & 2 & 0 & 1 \\ 
	\hline \Grayrow
		$L_{14}$ & -0 & 011 & 0 & 2 & 0 & -1 \\ 
	\hline
		$L_{15}$ & 0 & 011 & 4 & 0 & 0 & -1 \\ 
	\hline \Grayrow
		$L_{16}$ & 0 & 009 & 4 & 0 & 0 & -2 \\ 
	\hline
		$L_{17}$ & -0 & $008 \cdot e$ & 2 & 0 & 1 & -1 \\ 
	\hline \Grayrow
		$L_{18}$ & -0 & $007 \cdot e$ & 2 & 0 & 1 & 0 \\ 
	\hline
		$L_{19}$ & 0 & 005 & -1 & 0 & 0 & 1 \\ 
	\hline \Grayrow
		$L_{20}$ & 0 & $005 \cdot e$ & 1 & 0 & 1 & 0 \\ 
	\hline
\end{tabular}
\caption[Längenstörkoeffizienten der Mondbewegung]
{Längenstörkoeffizienten der Mondbewegung, Quelle: 
\cite[S. 237 Tab. 4.2-2]{SterneImComputer}}
\label{tab:Moonl}
\end{minipage}%
\begin{minipage}{.5\linewidth}
\centering
\begin{tabular}{|c|r<{,\kern-\tabcolsep}>{\kern-\tabcolsep}l|c|c|c|c|}
	\hline
		$B_N$ & \multicolumn{2}{c|}{$C_N$} & $i$ & $j$ & $k$ & $l$ \\
	\hline
	\EmptyRow
	\hline 
		$B_1$ & 5 & 128 & 0 & 1 & 0 & 0 \\ 
	\hline \Grayrow
		$B_2$ & 0 & 281 & 0 & 1 & 0 & 1 \\ 
	\hline
		$B_3$ & 0 & 278 & 0 & -1 & 0 & 1 \\ 
	\hline \Grayrow
		$B_4$ & 0 & 173 & 2 & -1 & 0 & 0 \\ 
	\hline
		$B_5$ & 0 & 055 & 2 & 1 & 0 & -1 \\ 
	\hline \Grayrow
		$B_6$ & 0 & 046 & 2 & -1 & 0 & -1 \\ 
	\hline
		$B_7$ & 0 & 033 & 2 & 1 & 0 & 0 \\ 
	\hline \Grayrow
		$B_8$ & 0 & 017 & 0 & 1 & 0 & 2 \\ 
	\hline
		$B_9$ & 0 & 009 & 2 & -1 & 0 & 1 \\ 
	\hline \Grayrow
		$B_{10}$ & 0 & 009 & 0 & -1 & 0 & 0 \\ 
	\hline
		$B_{11}$ & 0 & $008 \cdot e$ & 2 & -1 & -1 & 0 \\ 
	\hline
\end{tabular}
\caption[Breitenstörkoeffizienten der Mondbewegung]
{Breitenstörkoeffizienten der Mondbewegung, Quelle: 
\cite[S. 237 Tab. 4.2-2]{SterneImComputer}}
\label{tab:Moonb}
\end{minipage}
\end{table}

Um nun die Mondposition in Ekliptikalkoordinaten zu erhalten, ist die Summe 
der einzelnen Terme ($L_n$ und $B_n$) in \autoref{eq:MoonLambda} und 
\ref{eq:Moonbeta} einzusetzen.

\begin{equation} \label{eq:MoonLambda}
\Lambda = L + f + \sum(L_n)
\end{equation}

\begin{equation} \label{eq:Moonbeta}
\beta = \sum(B_n)
\end{equation}

\subsubsection{Ekliptikal zu äquatorialen Koordinaten}
Die hier dargestellte Transformation der Koordinaten ist allgemein gehalten 
und funktioniert auch für die Sonne, dabei muss wie oben schon erwähnt die 
Breite ($\beta$) auf null gesetzt werden.

Es wurden zwei unterschiedliche Verfahren implementiert, da so die Anzahl der 
ausgeführten Befehle, und damit auch die Laufzeit verringert werden konnte. 
Denn die Koordinatentransformation für die Sonne ist einfacher, da $\beta = 0$
ist. Aus diesem Grund werden auch beide Verfahren beschrieben.

\begin{equation} \label{eq:KoordEk2AeE}
\varepsilon = 23,452 - 0,013\,01 \cdot T - 0,000\,001\,6 \cdot T^2 + 0,000\,000\,5 \cdot T^3
\end{equation}

$\varepsilon$ ist wieder der Winkel der Ekliptik und $T$ kann aus 
\autoref{eq:STT0} entnommen werden \QuoteIndirect{SterneImComputer}
{S. 195, Gl. 3.8.14}.

\begin{align}
x &= \cos(\beta) \cdot \cos(\Lambda) \label{eq:KoordEk2Aex} \\
y &= \cos(\beta) \cdot \sin(\Lambda) \cdot \cos(\varepsilon) - \sin(\beta) \cdot \sin(\varepsilon) \label{eq:KoordEk2Aey} \\
z &= \cos(\beta) \cdot \sin(\Lambda) \cdot \sin(\varepsilon) + \sin(\beta) \cdot \cos(\varepsilon) \label{eq:KoordEk2Aez}
\end{align}

Die Hilfsgrößen $x$, $y$ und $z$ werden in \autoref{eq:KoordEk2Aex} bis 
\ref{eq:KoordEk2Aez} berechnet \QuoteIndirect{SterneImComputer}
{S. 195, Gl. 3.8.15 bis 3.8.17}.

\begin{equation} \label{eq:KoordEk2Aea}
\alpha = \arctan \left( \frac{y}{x} \right)
\end{equation}

Anschließend berechnen sich die Rektaszension ($\alpha$) und die Deklination 
($\delta$) aus \autoref{eq:KoordEk2Aea} und \ref{eq:KoordEk2Aed} 
\QuoteIndirect{SterneImComputer}{S. 195, Gl. 3.8.18 und 3.8.19}.

\begin{equation} \label{eq:KoordEk2Aed}
\delta = \arctan \left( \frac{z}{\sqrt{x^2+y^2}} \right)
\end{equation}

\subsubsection{Äquatorial zu horizontalen Koordinaten}
Die Berechnung des Stundenwinkels ($\tau$) findet sich in \autoref{eq:SunnTau}.
Danach werden die Hilfsgrößen $x$, $y$ und $z$ mit \autoref{eq:KoordAe2Hzx} 
bis \ref{eq:KoordAe2Hzz} berechnet, wobei der Breitengrad des 
Beobachtungsortes durch $\varphi$ symbolisiert wird
\QuoteIndirect{SterneImComputer}{S. 194 f., Gl. 3.8.8 bis 3.8.10}.

\begin{align}
x & = \sin(\tau) \cdot - \cos(\delta) \label{eq:KoordAe2Hzx} \\
y & = \sin(\delta) \cdot \cos(\varphi) - \cos(\delta) \cdot \cos(\tau) \cdot \sin(\varphi) \label{eq:KoordAe2Hzy} \\
z & = \sin(\delta) \cdot \sin(\varphi) + \cos(\delta) \cdot \cos(\tau) \cdot \cos(\varphi) \label{eq:KoordAe2Hzz}
\end{align}

Abschließend werden nun noch Azimut ($a$) und Höhe ($h$) mittels 
\autoref{eq:KoordAe2Hza} und \ref{eq:KoordAe2Hzh} bestimmt
\QuoteIndirect{SterneImComputer}{S. 195, Gl. 3.8.11 bis 3.8.12}.

\begin{equation} \label{eq:KoordAe2Hza}
a = \arctan \left( \frac{x}{y} \right)
\end{equation}

Nun kann noch, wie bei der Sonne, auch eine Korrektur der sichtbaren Höhe durch 
die Refraktion durchgeführt werden \SeeS{subsubsec:sunRef}.

\begin{equation} \label{eq:KoordAe2Hzh}
h =  \arctan \left( \frac{z}{\sqrt{x^2+y^2}} \right)
\end{equation}

Über eine Timeline aus $86\,400$ Sekunden wird ein permanentes Fortrücken der 
Zeit eines Tages erreicht. Diese Timeline lässt sich beschleunigen und 
verlangsamen, somit kann auch die Zeit beschleunigt/verlangsamt werden. Am 
Ende eines virtuellen Tages wird der Tag im Monat inkrementiert. Am letzten 
Tag im Monat wird der Monat inkrementiert und mit dem Jahr wird ebenso 
verfahren. \index{Timeline}

\subsection{Skysphere}
Die \ac{UE4} bietet in ihren Beispiel-Projekten eine Skysphere, also eine 
Kugel, die die virtuelle Welt umgibt und den Himmel darstellt, an. Diese 
Skysphere wird als Basis für den Himmel des Planungstools benutzt. Die Farbe 
des Himmels wird in Abhängigkeit von der Höhe der Sonne gesteuert.

\medskip
Dabei wird der Himmel und auch die Sonne als Textur auf die Innenseite dieser 
Kugel projiziert. Für die Sonne ist dieses Verfahren gut geeignet, da sie 
keine erkennbare Textur aufweist, sie erscheint als ein gelb-weißer Stern. Bei 
dem Mond ist das anders, da z.B. die Krater mit bloßem Auge zu erkennen sind. 
Diese Oberfläche lässt sich schlecht in die Skysphere-Textur einbinden, da 
sich die Krater sichtbar verzerren, je nachdem, wo der Mond am Horizont zu 
sehen ist. Nachts werden Sterne als Textur eingeblendet.

Aus diesem Grund wird der Mond und auch die Sonne als Kugel mit eigener Textur 
dargestellt. Wobei diese primitiven Objekte kein Licht emittieren, dazu gibt 
es \QuoteM{directional lights}, die eine unendlich ferne Lichtquelle 
simulieren. Directional light und simulierte Himmelskörper werden an die 
entsprechende Position rotiert, so wirkt es, als wenn die Himmelskörper das 
Licht emittieren würde. Da bei Neumond der Mond kein Licht emittiert wird über 
das zuvor berechnete Mondalter eine obere Grenze für die Helligkeit des 
Mondlichtes festgelegt. Die virtuelle Mondphase ergibt sich von selbst, 
indem die Transparenz des Mond-Materials von der Richtung des Sonnenlichtes 
gesteuert wird. So entsteht eine Mondphase genau wie in der Realität auch. 
\index{Directional light}

\medskip
In der Realität durch diffuse Streuung erzeugtes Umgebungslicht wird in der 
\ac{UE4} durch ein \QuoteM{Sky light} simuliert. Die Farbe dieses Sky Light 
muss sich an die des Himmels anpassen, sonst wirkt die Szene unrealistisch. 
Leider benötigt eine Neuberechnung des Sky Lights soviel Zeit, das es zu einem  
merklichen Einfrieren der Szene kommt. Auch kann die \ac{UE4} nur ein Sky 
light pro Szene verarbeiten. Glücklicherweise lässt sich ein \ac{HDR}-Bild als 
Basis für das Sky Light benutzen. Dieses \ac{HDR} lässt sich einfärben. 
Dieser Umstand wird benutzt, um zu jeder Tageszeit die passende Farbe zu 
erhalten. Ein Farbgradient wird benutzt, um die Farbe über den Tagesverlauf 
anzupassen, die Helligkeit wird über den Alpha-Kanal der Farbgradient 
gesteuert. \index{Sky Light}

\medskip
Es ist sinnvoll, sowohl die simulierten Himmelskörper als auch das dazugehörige 
Licht auszublenden, sobald sich der Himmelskörper unter dem Horizont 
befindet. Wenn dies nicht geschieht, kann es passieren, dass Licht durch 
unsauber modellierte Teile der Karte nach oben scheint und unrealistisch 
wirkt.

\subsection{Wochentage}
Um \ref{text:DaysOfWeek} zu erfüllen, muss das Planungstool aus einem gegebenen 
Datum den entsprechenden Wochentag generieren können. Dazu wird der 
Algorithmus von Tomohiko Sakamoto benutzt \QuoteIndirectNoPage{DayOfWeeks}. 
Dieser Algorithmus arbeitet nur mit dem Gregorianischem Kalender, dadurch 
werden die Tage vor dem 15. Oktober 1582 falsch berechnet. Da es zu dieser Zeit 
keine Multicopter gab, wird dieser Fehler nicht weiter beachtet. Wenn die Zeit 
zu stark beschleunigt wird, bricht die \ac{UE4} mit dem originalen Algorithmus 
ab und zeigt eine Fehlermeldung. Aus diesem Grunde wurde der Algorithmus leicht 
verändert. \autoref{code:DayOfWeek.c} stellt den veränderten Algorithmus in 
C++ beispielhaft dar. \index{Tomohiko Sakamoto}

\begin{c++}{DayOfWeek.c}{code:DayOfWeek.c}
DayOfWeek(y, m, d) {
	static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
	int year_offset = 0;
	
	if (m < 3) {
	year_offset = -1;
	}
	
	y = y + year_offset;
	return (y + y/4 - y/100 + y/400 + t[m-1] + d) % 7;
}
\end{c++}

\subsection{Lat Long Position im \ac{UI}}
\label{subsec:Pos}
Um \ref{text:WGS84} zu erfüllen, wird Längen- und Breitengrad des 
Koordinatenursprungs der Szene gespeichert. Danach wird die Entfernung des 
Multicopters in Metern auf der X- und Y-Achse berechnet. Anschließend wird 
diese Distanz, in Längen- und Breitengrad umgerechnet, auf den gespeicherten 
Wert addiert. Zur Umrechnung von Metern in Längen- und Breitengrad wurden zwei 
Algorithmen implementiert, von denen nur einer benutzt und beschrieben werden 
soll. Der beschriebene Algorithmus arbeitet genauer und wird in 
\autoref{eq:radius} bis \ref{eq:Long} dargestellt 
\QuoteIndirectNoPage{LatLong1}.

\begin{align}
R & = 6\,371\,000 \label{eq:radius} \\
Lat & = \frac{D_N}{R} \cdot \frac{180}{\pi} \label{eq:Lat} \\
Lon & = \frac{D_E}{R \cdot \cos \left(Lat \cdot \frac{180}{\pi} \right) \cdot \frac{180}{\pi}} \label{eq:Long} 
\end{align}

Hierbei stellt $R$, $D_N$ und $D_E$ den Erdradius, die Entfernung auf der Nord-
Süd-Achse (Norden $\rightarrow$ Positiv) und die Entfernung auf der Ost-West-
Achse (Osten $\rightarrow$ Positiv) dar.

Der nicht benutzte Algorithmus findet sich unter \cite{LatLong}.