\label{sec:Maps}
\ac{3D}-Karten in guter Qualität zu erstellen, ist die größte Herausforderung 
in der Erstellung einer Planungssoftware für Multicopterflüge. Qualitativ 
hochwertige \ac{3D}-Karten müssen sowohl die Gelände- als auch die 
Gebäudehöhe wiedergeben. Auch die Form der Gebäude muss der Realität 
entsprechen. Infrastrukturelemente wie z.B. Straßen, Kanäle, 
Hochspannungsmasten, Schienen, Tunnel und Brücken sollten als solche 
erkennbar sein. Das Gleiche gilt für Flüsse, Seen und Meere. Eine 
ansprechende Darstellung mit Texturen ist wünschenswert. Um solche 
\ac{3D}-Karten zu erstellen, gibt es mehrere Möglichkeiten, von denen drei im 
Folgenden genauer beschrieben werden sollen.

\subsection{Open Street Map}
Das Ziel des \ac{OSM}-Projektes ist es, Karten frei für jedermann zur 
Verfügung zu stellen. Eine große Nutzergemeinde \QuoteIndirectNoPage{OSMUser} 
trägt zu diesem Zweck die Daten aus unterschiedlichen Quellen, wie z.B. freie 
Datenbanken, Satellitenaufnahmen und selbst gesammelten \ac{GPS}-log-Daten, 
zusammen \QuoteIndirectNoPage{OSMAbout}. Dabei ist es auch möglich, 
\ac{3D}-Daten der Umgebung zu sammeln \QuoteIndirectNoPage{OSM3D}. Ein 
Vorteil von \ac{OSM} gegenüber z.B. den \ac{3D}-Modellen von Google Earth 
ist, dass die \ac{OSM}-Daten nicht nur für Städte verfügbar sind, sondern 
auch für kleine Ortschaften. Je nach Datenlage ist so die ganze Welt als 
\ac{3D}-Modell erzeugbar.

\medskip
Über verschiedene Webseiten \QuoteIndirectNoPage{OSMDownload, OSMDownload2} 
können diese Daten heruntergeladen werden. Dazu sind zwei Formate üblich, zum 
einen \texttt{.osm}, \QuoteIndirectNoPage{OSMXML} das eine nach einem 
bestimmten Schema aufgebaute \ac{XML}-Datei ist, und zum anderen 
\texttt{.osm.pbf}. Das \ac{PBF}-Format enthält die gleichen Daten, ist aber 
als Binary auf Speicher- und Laufzeiteffizienz optimiert. 
\QuoteIndirectNoPage{OSMPBF}. \index{Format!PBF} \index{Format!OSM}

\medskip
Um aus diesen Daten nun \ac{3D}-Karten zu generieren, sind mehrere Schritte 
notwendig, die nun im einzelnen beschrieben werden sollen.
\autoref{fig:OSM2UE4} stellt diesen Vorgang graphisch dar, die 
Kantenbeschriftung gibt das Dateiformat wieder und die Knoten die benutzte 
Software.

\begin{figure}[htb]
	%\Centerfloat
	\centering
	\includegraphics[scale=0.47]{Content/Implementation/Maps/osm2ue4.png}
	\caption{Der Weg vom \ac{OSM} zur \ac{UE4}}
	\label{fig:OSM2UE4}
\end{figure}

\subsubsection{Splitter \& OsmConvert}
Die \ac{UE4} ist mit einer einzigen Karte, die das gesamte geforderte Gebiet 
(\ref{text:MapHanMin}) abdeckt, überfordert, da die komplette Karte im 
Hauptspeicher des \ac{PC}s liegen muss und die Berechnung der Grafik dadurch 
extrem verlangsamt wird. Deswegen ist es üblich, eine große Welt aus vielen 
kleinen Stücken zusammenzubauen, dieser Vorgang nennt sich Tiling.

Erschwerend kommt noch hinzu, dass der in \ref{text:MapHanMin} definierte 
Bereich zwei Bundesländer einschließt, also sind Karten von den einzelnen 
Bundesländern nicht ausreichend. Deswegen muss eine Karte von komplett 
Deutschland benutzt werden, die wiederum zu groß ist. Somit muss ein Tiling 
durchgeführt werden.

\medskip
Dazu gibt es viele unterschiedliche Werkzeuge, von den zwei vorgestellt 
werden sollen: zum einen Splitter \QuoteIndirectNoPage{OSMSplitter} und zum 
anderen OsmConvert \QuoteIndirectNoPage{OSMConvert}.

\paragraph{Splitter}
Zunächst soll Splitter genauer betrachtet werden. Dieses Tool ist eigentlich 
dazu entwickelt worden, um \ac{OSM}-Karten für Geräte des Herstellers Garmin 
zu optimieren, da diese Geräte eine maximale Größe für Karten haben. Aus 
diesem Grunde wird die Größe der resultierenden Tiles in \QuoteM{Nodes} 
angegeben. Dies ist nützlich, da so urbane Gebiete, die mehr Einträge pro 
Fläche enthalten, in kleinere Tiles unterteilt werden als rurale Gebiete. 
Dies ist zwar für die \ac{UE4} von Vorteil, da jedes spätere Kartenstück 
somit (ungefähr) gleich viel Speicherplatz belegt. Dies hat aber für den 
Ersteller der Karten den Nachteil, dass er die Karte aus unterschiedlich großen 
Einzelflächen zusammenbauen muss.

Splitter ist in Java geschrieben und gibt \ac{PBF}-Daten aus. Eine Karte von 
Europa in 1078 Tiles aufzuteilen dauert mit einem Intel Core i7-3770K und 12 
\ac{GB} Hauptspeicher für diesen Prozess 46.014 Sekunden, was mehr als zwölf 
Stunden sind (\autoref{code:Splitter.sh}: Zeile 1--3). Dieses Vorgehen 
wurde gewählt, um den Verarbeitungsweg an einer möglichst großen Karte zu 
testen (\ref{text:MapWorld}). Die resultierenden Tiles sind zu groß für die 
\ac{UE4}, deswegen werden die Tiles, die den Zielbereich 
(\ref{text:MapHanMin}) darstellen, nochmals unterteilt 
(\autoref{code:Splitter.sh}: Zeile 5--7). So ist es möglich, die passenden 
Werte für \texttt{--max-nodes=} zu finden, ohne bei jedem Test erneut die 
große Europakarte zerteilen zu müssen.

\begin{bash}{Aufrufe von Splitter}{code:Splitter.sh}
java -Xmx12288m -jar splitter.jar --max-nodes=1600000 
	--output-dir=/data2/Maps/EuropaSplitt/
	/data2/Maps/Europa/europe-latest.osm

java -Xmx12288m -jar splitter.jar --max-nodes=160000 
	--output-dir=/data2/Maps/EuropaSplitt/SHG/
	/data2/Maps/EuropaSplitt/63240887.osm.pbf 
\end{bash}

Zur besseren Darstellung wurden die Aufrufe in jeweils drei Zeilen 
aufgeteilt, in diesem Format sind die Aufrufe nicht lauffähig. Die Dateipfade 
sind beispielhaft.

\paragraph{OsmConvert}
Nun soll OsmConvert betrachtet werden. Dieses Tool ist in C geschrieben und 
unterstützt verschiedene Ein- und Ausgabeformate, unter anderem \texttt{.osm} 
und \texttt{.osm.pbf.} Der für diese Anwendung wichtigste Unterschied zu 
Splitter liegt darin, dass OsmConvert die Tiles nicht nach Anzahl der Nodes, 
sondern nach Längen und Breitengrad aufteilt. So ist es möglich, immer gleich 
große Flächen zu erzeugen. Leider bietet dieses Programm keine Möglichkeit, 
mehrere Tiles auf einmal zu erzeugen. Um dieses Problem zu lösen, wurde ein 
\ac{Bash}-Script geschrieben. Die Laufzeit des Programms hängt von der Größe 
der Ausgangskarte ab, deswegen ist es sinnvoll mittels:

\texttt{./osmconvert32 europe-latest.osm -b=8.7,52.1,10.1,52.6 -o=HAN\_MIN.osm}

eine Karte mit den minimal notwendigen Abmessungen zu erstellen. Diese kann 
dann mit dem \ac{Bash}-Script weiter unterteilt werden 
\SeeS{code:OSMConvert.sh}.
Um das Zerteilen weiter zu beschleunigen, ist es sinnvoll, mehrere 
Konvertierungen parallel laufen zu lassen. Dazu bietet es sich an, die GNU 
Parallel Bibliothek zu benutzen \QuoteIndirectNoPage{GNUParalel}. Die 
\ac{Bash} bietet leider keine Additionen für Fließkommazahlen an, deswegen 
wird auf Python zurückgegriffen. \index{Python} \index{Bash}

\begin{bash}{OSMConvert.sh}{code:OSMConvert.sh}
#!/bin/sh
pathToOsmConvert=/data2/Maps/osmconvert32
pathToMap=/data2/Maps/HAN_MIN.osm
pathToResult=/data2/Maps/Tiles
beginLat=52.2
beginLong=8.8
endLat=52.5
endLong=10
step=0.1
export LC_NUMERIC="en_US.UTF-8" # to get a point as separator
mkdir -p $pathToResult

for i in $(seq $beginLong $step $(python -c "print $endLong-$step"))
  do
  for j in $(seq $beginLat $step $(python -c "print $endLat-$step"))
    do
     sem -j+0 $pathToOsmConvert $pathToMap 
     -b=$i,$j,$(python -c "print $i+$step"),$(python -c "print $j+$step") 
     --complete-ways -o=$pathToResult/
     $j,$i-$(python -c "print $j+$step"),$(python -c "print $i+$step").osm 
     2> /dev/null
     
     echo Make Tile From 
     $(python -c "print $j+$step"), $(python -c "print $i+$step") to $j, $i
    done
done
sem --wait
\end{bash}

Auch hier wurde der zentrale Aufruf (Zeile 24--29) und der Print auf die 
Konsole (Zeile 31--32) zur besseren Übersicht aufgeteilt. Damit das Script 
lauffähig wird, müssen diese Aufrufe wieder zusammengeführt werden. Die 
Dateipfade sind beispielhaft.

\subsubsection{\ac{SRTM}-Daten} \label{subsubsec:SRTM}
Um die Anforderung \ref{text:HGT} erfüllen zu können, ist es notwendig, Daten 
über die Beschaffenheit des jeweiligen Geländes bereitzustellen und zu
verarbeiten. Um die spätere Weiterverarbeitung zu ermöglichen, müssen diese 
Daten im \texttt{.hgt}-Format vorliegen. 

\medskip
Solche Daten (zwischen 60. nördlichen und 56. südlichen Breitengrad) wurden 
von der \ac{NASA} im Rahmen der \ac{STS-99}-Mission im Februar 2000 gesammelt 
\QuoteIndirectNoPage{SRTMMission1, SRTMMission} und sind als Public 
Domain für jedermann erhältlich \QuoteIndirectNoPage{SRTMLicence, 
SRTMLicence1}.

\medskip
\index{STS-99}Die so gewonnenen Rohdaten sind in verschiedenen Versionen mit 
verschiedenen Auflösungen im Umlauf.

Da solche Daten auch für militärische Zwecke benutzt werden, waren bis 2014 
nur Daten mit einer Auflösung von drei Bogensekunden (\ac{SRTM}3) verfügbar. 
Daten mit einer Auflösung von einer Bogensekunde (\ac{SRTM}1) wurden seit 
September 2014 veröffentlicht \QuoteIndirectNoPage{SRTM1}.

Version 1 sind die unbearbeiteten Originaldaten. Diese Daten enthalten 
Fehler, z.B. Gewässer mit unterschiedlichen Höhenangaben, steile Berghänge 
enthalten Fehlstellen aufgrund des Messverfahrens und die Küstenlinien 
entsprechen nicht der Realität. All diese Fehler wurden mittels anderer 
Informationsquellen berichtigt und als Version 2 veröffentlicht. Beide 
Versionen bieten eine Auflösung von drei Bogensekunden. In Version 2.1 wurde 
zusätzlich die Berechnung der \ac{SRTM}3-Felder aus den \ac{SRTM}1-Daten 
verbessert \QuoteIndirectNoPage{SRTMVersions}. 

Bei den neuen \ac{SRTM}1-Daten sind die Löcher gefüllt, somit 
entsprechen sie Version 2 \QuoteIndirectNoPage{SRTM1Versions}.
\index{SRTM!Version 1} \index{SRTM!Version 2} \index{SRTM!Version 2.1}

\medskip
Die Daten werden in jeweils 1$\times$1 Grad-Stücke als \texttt{.hgt} zum 
Download angeboten. Dabei gibt der Name den Längen- und Breitengrad im 
\ac{WGS 84}-System der linken unteren Ecke an. Die \ac{SRTM}1-Daten 
enthalten 3601$\times$3601 Bildpunkte, deren 16-Bit-Werte (signed big-endian 
integer) die Höhe über \ac{NN} in Metern angibt. Die \ac{SRTM}3-Daten 
sind mit 1201$\times$1201 Pixeln aufgelöst. Leere Stellen 
in den Version-1-Daten sind durch eine Höhe von -32768 m über \ac{NN} 
gekennzeichnet. \QuoteIndirectNoPage{SRTM1Versions, SRTMFileFormat}.

\medskip
Für den einfachen Download dieser Daten ist es notwendig, ein Script zu 
schreiben, denn leider blockieren die Server einen automatischen Download von 
mehr als einer Datei. Um nicht jede Datei einzeln herunterladen zu müssen, 
kann ein Script geschrieben werden, dass auf Basis der \texttt{index.html} 
eine Liste mit \texttt{wget}-Befehlen erzeugt. Dies soll am Beispiel der 
\ac{SRTM}3-Daten in der Version 2.1 gezeigt werden (siehe 
\autoref{code:MakeGetZip.sh}).

\clearpage

\begin{bash}{MakeGetZip.sh}{code:MakeGetZip.sh}
for i in `grep href index.html |awk  '{print $3}'|cut -d\< -f1`; 
	do echo wget http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/$i; 	
	done > getzips
\end{bash}

\autoref{code:GetZip.sh} zeigt einen Ausschnitt aus dem generierten 
Download-File.

\begin{bash}{GetZip.sh}{code:GetZip.sh}
wget http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/N00E072.hgt.zip
wget http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/N00E073.hgt.zip
wget http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/N00E097.hgt.zip
wget http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/N00E098.hgt.zip
wget http://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Eurasia/N00E099.hgt.zip
\end{bash}

Unter \cite{SRTM1Get} können die \ac{SRTM}1-Daten und unter \cite{SRTM3Get} die \ac{SRTM}3-Daten bezogen werden. Anschließen müssen sie noch dekomprimiert werden, z.B. mit \index{SRTM!Herunterladen}

\texttt{for i in * ;do unzip \$i ; done}.

\subsubsection{OSM2World} \label{subsec:OSM2World}
Der Kern des Weges von \ac{OSM} zur \ac{UE4} ist OSM2World. Dieses Programm 
konvertiert die \ac{OSM}- (sowohl \texttt{.osm} als auch 
\texttt{.osm.pbf}) und die \texttt{.hgt}-Daten in Wavefront OBJ-Daten mit 
.\ac{mtl}-File \QuoteIndirectNoPage{OSM2WorldWikiPage, OSM2WorldPage}. Es ist 
in Java geschrieben und bietet einen \ac{GUI}- und einen 
Kommandozeilen-Modus. Auch der Export in andere Dateiformate z.B. 
\ac{PNG}-Bilder ist möglich, wird im Rahmen dieser Arbeit aber nicht weiter 
beachtet. Um die Fähigkeiten von OSM2World zu zeigen, wurde eine Karte von 
einigen europäischen Ländern generiert \QuoteIndirectNoPage{OSM2SlippyMap}. 
\index{Format!OBJ}

Das Programm lässt sich über ein \ac{Bash}-Script (\texttt{osm2world.sh}) 
starten. Es ist sinnvoll, in diesem Script der Java-\ac{VM} mehr als die 
angegeben 2 \ac{GB} Hauptspeicher zuzuteilen 
(\texttt{vmparams=\grqq -Xmx12G\grqq}).

\medskip
Die Eigenschaften des Programms werden in der 
\texttt{texture\_config.properties} festgelegt. Solche Eigenschaften sind 
z.B. der Speicherort der \ac{SRTM}-Daten, die Farbe und Textur der einzelne 
Flächen (Wasser, Straßen, Häuser), die Baumdichte in Wäldern und ob die Bäume 
als Polygone oder als Sprites dargestellt werden sollen 
\QuoteIndirectNoPage{OSM2WorldConfig}. Sprites sind in diesem Fall zwei um 
$90^\circ$ gedrehte, ineinander liegende Flächen, auf die eine Textur 
projiziert wird. \index{Sprites}

\medskip
Aufgerufen wird das Programm wie folgt:

\begin{bash}{Osm2Obj.sh}{code:Osm2Obj.sh}
for i in /data2/Maps/Tiles/*
	do ./osm2world.sh -i $i -o $i.obj
	done
\end{bash}

\medskip
Leider entstehen bei der Benutzung von OSM2World noch einige Probleme, diese 
sollen nun besprochen werden.

In den erzeugten \texttt{.obj}-Daten finden sich Stellen, an denen der Boden 
fehlt. Dies wird in \autoref{fig:Hole} dargestellt. Durch das Loch ist die 
SkySphere sichtbar, so kommt die orange Farbe zustande. An diesem Bild kann man 
auch gut die Sprite-Bäume in schwarz erkennen. Ein anderes Problem wird auch 
sichtbar, in vielen Regionen ist die Datenlage für \ac{3D}-Objekte nicht gut. 
So haben z.B. alle Gebäude die gleiche Höhe und ein Flachdach, da in der 
\ac{OSM}-Datei die entsprechenden Daten fehlen. Auf dem Bild ist auch die 
Geländeform gut zu erkennen.

\begin{figure}[htb]
	%\Centerfloat
	\centering
	\includegraphics[scale=0.22]{Content/Implementation/Maps/Hole.png}
	\caption{Löcher im Boden einer von OSM2World generierten 
			 \texttt{.obj}-Datei}
	\label{fig:Hole}
\end{figure}

Während des Konvertierens von \texttt{.osm} nach \texttt{.obj} können je nach 
benutzten \ac{OSM}-Daten viele Fehlermeldungen ausgegeben werden. Diese 
Meldungen beziehen sich auf lokale \ac{OSM}-Elemente und können ignoriert 
werden.

\medskip
Ein großes Problem stellt die Tatsache dar, dass sich die resultierenden 
\texttt{.obj}-Daten nur mit wenigen Programmen weiterverarbeiten lassen.
Autodesk Maya z.B. bricht den Import mit folgender Fehlermeldung ab:

\begin{sloppypar}
\texttt{// Error: line 0: OBJ file line 5029: index out of range for face 
creation. //}
\end{sloppypar}

\subsubsection{Blender \& \acl{UE4}}

Auch die \ac{UE4} kann nicht alle generierten \texttt{.obj}-Daten 
importieren, dieser Umstand ist von Epic Games als Fehler anerkannt und wird 
hoffentlich in Zukunft behoben werden \QuoteIndirectNoPage{UE4ImportCrash}.
Da die \ac{UE4} auf den Import von \texttt{.fbx}-Daten (Ein Austauschformat 
für \ac{3D}-Modelle von Autodesk) optimiert ist 
\QuoteIndirectNoPage{UE4Import} und OSM2World entwickelt wurde, um die 
resultierenden \texttt{.obj}-Daten nach Blender zu exportieren, ist es 
sinnvoll, die \texttt{.obj}-Daten mit Blender in \texttt{.fbx}-Daten zu 
konvertieren. Durch dieses Vorgehen ergeben sich zwar mehr Möglichkeiten, zum 
anderen stellt es ein weiteres Glied in der Kette dar, wodurch der Vorgang 
komplexer wird. Im Folgenden soll dies Vorgehen genauer beschrieben werden. 
\index{Format!FBX}

\medskip
Blender (\url{https://www.blender.org/}) ist eine unter \ac{GPL} 
\QuoteIndirectNoPage{GPL} veröffentlichte Software für 
\ac{3D}-Grafikanwendungen. Eine der ersten Versionen wurde 2003 veröffentlicht, 
die aktuelle Version ist 2.76b vom 4. November 2015 
\QuoteIndirectNoPage{BlenderReleas}. Es ist unter Windows, Mac OS~X und Linux 
lauffähig.

\medskip
\autoref{code:Obj2Fbx.py} zeigt den ersten Entwurf eines Python-Scripts, mit 
dem sich eine Konvertierung von \texttt{.obj} nach \texttt{.fbx} 
automatisieren lässt. Jedoch nützt dieses Script momentan wenig, da sich auch 
die \texttt{.fbx}-Dateien nur schlecht in die \ac{UE4} importieren lassen. 
Darüber hinaus ist das Script suboptimal, da zu jeder \texttt{.fbx}-Datei mit 
Inhalt ein zweites \texttt{.fbx}-File erzeugt wird, das die 
Blender-Standardszene enthält. Dieses Script wird nur der Vollständigkeit 
halber beschrieben.

\medskip
Sind die Karten-Tiles einmal in die \ac{UE4} importiert, müssen sie so 
ausgerichtet werden, dass sich eine zusammenhängende Karte ergibt. Zusätzlich 
müssen die Tiles als Streaming Level erzeugt werden. Diese sorgen dafür, dass 
nur der momentan sichtbare Teil der Karte im Hauptspeicher geladen ist. 
Werden die einzelnen Tiles alle in das selbe Level importiert, sind alle 
Tiles auf einmal geladen und das Tiling wäre sinnlos 
\QuoteIndirectNoPage{UE4LevelStreaming}.

\medskip
In der Implementierung zu dieser Bachelorarbeit finden sich zwei Karten, die 
auf diesem Wege erstellt wurden, zum einen \QuoteM{PW} und zum anderen 
\QuoteM{SHG}.

Die \QuoteM{SHG}-Karte stellt den Bereich zwischen 52,338867 N
9,096680 O und 52,514648 N 9,360352 O dar. Dieser Bereich ist zu groß für ein 
einzelnes Teilstück. In diese Karte wurden keine \ac{SRTM}-Daten eingebunden, 
so dass auch keine Höhendaten vorhanden sind.

Die \QuoteM{PW}-Karte stellt den Bereich zwischen 52,230587 N
8,882073 O und 52,58636 N 8,963142 O dar. \autoref{fig:Hole} zeigt einen Teil 
dieser Karte. In diese Karte wurden \ac{SRTM}-Daten eingebunden, wie man auch 
im Hintergrund sehen kann.

\clearpage
\begin{python}{Obj2Fbx.py}{code:Obj2Fbx.py}
import os
import bpy

# Execute with: blender --background --python Obj2Fbx.py
path_to_directory = "/data2/Maps/Tiles"
path_to_out_directory = "/data2/Maps/Tiles/fbx"
file_list = os.listdir(path_to_directory)
obj_list = [item for item in file_list if item[-3:] == 'obj']

for item in obj_list:
    path_to_file = os.path.join(path_to_directory, item)
    bpy.ops.scene.new(type='NEW')
    bpy.ops.import_scene.obj(
			filepath = path_to_file, use_smooth_groups = True,
			axis_forward = '-Z', axis_up = 'Y',
			use_image_search = True, filter_glob = "*.obj;*.mtl")
    bpy.ops.export_scene.fbx(
			filepath = os.path.join(path_to_out_directory, item),
			batch_mode = 'SCENE', mesh_smooth_type = 'FACE',
			object_types = {'MESH'}, use_batch_own_dir = False,
			check_existing = True, filter_glob = "*.fbx",
			axis_forward = '-Z', axis_up = 'Y', 
			use_mesh_modifiers = True, 
			use_mesh_edges = False, use_default_take = True,
			path_mode = 'AUTO', use_metadata = True)
    bpy.ops.scene.delete()
\end{python} \index{Python}

\subsection{\acl{CityGML}}
Um \ac{3D}-Karten zu erstellen, kann man auch auf \ac{CityGML}-Daten 
zurückzugreifen. \ac{CityGML} ist eine \ac{XML}-basierte 
Auszeichnungssprache, die von der Open Geospatial Consortium standardisiert 
wird \QuoteIndirectNoPage{CityGMLPage, CityGMLStandard}.

Als Quelle von solchen \ac{CityGML}-Daten können z.B. Landesvermessungsämter 
benutzt werden \QuoteIndirectNoPage{CityGMLHannover, CityGMLBerlin}.

Die \ac{UE4} kann diese \ac{CityGML}-Daten jedoch nicht importieren, deswegen 
müssen diese Daten zunächst in andere Formate überführt werden. Dazu bieten 
sich zwei Formate an, zum einen \texttt{.obj} und zum anderen \texttt{.fbx}, 
wobei \texttt{.fbx} zu bevorzugen ist \QuoteIndirectNoPage{UE4Import}. Um die 
Karten nun von \ac{CityGML} nach \texttt{.fbx} oder \texttt{.obj} zu 
konvertieren, gibt es verschiedene Wege.

\subsubsection{ESRI}
\ac{ESRI} ist ein US-amerikanisches Unternehmen, das Software und ein 
\ac{GIS} herstellt. Dieses Unternehmen bietet die \ac{ESRI} CityEngine an, 
die Kartendaten ins \texttt{.fbx} Format konvertieren kann 
\QuoteIndirectNoPage{CityEngineFBX, EsriCityEngine}.

Leider kann diese CityEngine keine \ac{CityGML}-Daten importieren.
Jedoch gibt es ein anderes Werkzeug von \ac{ESRI}, mit dem sich 
\ac{CityGML}-Daten in \ac{3DCIM}-Daten konvertieren lassen. Diese Daten kann 
man dann in die CityEngine importieren und so nach \texttt{.fbx} konvertieren. 
Dieses Werkzeug, der ArcCatalog, ist Teil der ArcGIS-Familie, innerhalb dieser 
Familie ist der ArcCatalog zur Verwaltung und zum Konvertieren von Daten 
gedacht. Sowohl die CityEngine als auch die ArcGIS-Familie können als 
Demoversion mit zeitlicher Beschränkung heruntergeladen werden. Auch der 
ArcCatalog kann in der Basisinstallation keine \ac{CityGML}-Daten 
verarbeiten, dazu sind zusätzliche Daten notwendig, die unter 
\cite{CityGMLImport2} angeboten werden. In \cite{CityGMLImport} und 
\cite{CityGMLImport1} wird beschrieben, wie diese Daten einzubinden sind.

\medskip
Leider lassen sich bei der aktuellen Version 10.3.1 diese Daten zwar 
einbinden, jedoch werden sie als inhaltslos angezeigt 
\QuoteIndirectNoPage{EsriError}. Dies führt dazu, dass sich keine 
\ac{CityGML}-Daten in die \ac{ESRI} CityEngine importieren lassen. Jedoch 
bietet die CityEngine eine Demo-Stadt an, mit der der Export zur \ac{UE4} 
getestet werden kann. \autoref{fig:CityEngine} stellt die CityEngine mit 
dieser Demo-Stadt dar. Beim Import der \texttt{.fbx}-Daten in die \ac{UE4} 
kommt es zu kleineren Problemen, da einige Texturen falsch berechnet werden, 
z.B. fehlen bei den Bäumen, die als Sprites dargestellt werden, der 
Alpha-Kanal, sodass der Bereich, der transparent sein sollte, durch Wiederholen 
des Farbwertes des äußersten regulär dargestellten Pixels gefüllt wird.
Diese lassen sich leicht beheben. \index{Sprites}

\begin{figure}[htb]
	%\Centerfloat
	\centering
	\includegraphics[scale=0.22]{Content/Implementation/Maps/Esri.png}
	\caption{Bildschirmphoto der \ac{ESRI} CityEngine}
	\label{fig:CityEngine}
\end{figure}

\medskip
In der Implementierung zu dieser Bachelorarbeit ist ein Teil dieser 
Demo-Stadt als \QuoteM{Esri}-Karte eingebunden. Durch entsprechenden Einsatz 
von \ac{LOD} \QuoteIndirectNoPage{UE4LOD} und Streaming Level 
\QuoteIndirectNoPage{UE4LevelStreaming} sollte es möglich sein, die gesamte 
Demo-Stadt in die \ac{UE4} zu übertragen.

\medskip
\ac{LOD} beschreibt die Darstellung eines Objektes in der virtuellen Welt 
durch mehrere unterschiedlich genau aufgelöste Objekte. So wird, wenn ein 
Objekt weit von der Kamera entfernt ist, nur eine niedrig aufgelöste Version 
des Objektes benutzt. Je näher sich Objekt und Kamera kommen, je detailreicher 
wird das Objekt dargestellt. Durch dieses Vorgehen lässt sich die Komplexität 
der \ac{3D}-Szene verringern und die Darstellung beschleunigen. \index{LOD}

\subsubsection{Citygml-to-obj \& CityGML2OBJs}
Zwei andere Programme, die \ac{CityGML}-Daten in das \texttt{.obj}-Format 
konvertieren, sind citygml-to-obj und City"-GML"-2"-OBJs. Beide werden nur 
der Vollständigkeit halber erwähnt, da sie sich für diese Anwendung nur bedingt 
eignen. Beide sind unter der MIT-Lizenz \QuoteIndirectNoPage{MIT} 
veröffentlicht. Citygml-to-obj ist in Javascript und City"-GML"-2"-OBJs in 
Python geschrieben. Leider erzeugt Citygml-to-obj für jedes virtuelle Gebäude 
eine eigene \texttt{.obj}-Datei und bei der Konvertierung mittels 
City"-GML"-2"-OBJs gehen die Texturen verloren 
\QuoteIndirectNoPage{Citygml2Obj, CityGML2OBJs, Biljecki:2015vk}. 

\medskip
Somit eignet sich CityGML2OBJs schlecht für diese Anwendung, da Texturen ein 
wichtiger Bestandteil einer \ac{3D}-Karte sind. Auch citygml-to-obj ist 
ungeeignet, da für jedes Gebäude eine eigene \texttt{.obj}-Datei angelegt 
wird, so kommen sehr viele \texttt{.obj}-Dateien zusammen. Jedes Gebäude muss 
dann einzeln an seinen Platz in der virtuellen Welt transformiert werden, 
was bei der hohen Anzahl an Gebäuden \ref{text:MapUpdate} unerfüllbar macht.

\subsection{Manuelles Modellieren}
Eine andere Möglichkeit \ac{3D}-Karten zu generieren, die an dieser Stelle 
beschrieben werden soll, ist es, die \ac{3D}-Karten manuell zu modellieren.
Dabei werden mit Hilfe von \ac{3D}-Anwendungen wie z.B. Autodesk Maya, 
SketchUp, 3ds Max oder Blender die entsprechenden Karten und Gebäude erstellt. 
Auch können Gebäude, die andere Personen modelliert haben, über 
Onlineplattformen benutzt werden \QuoteIndirectNoPage{3dwarehouse}.

\medskip
\ac{3D}-Karten, die durch dieses Verfahren entstanden sind, besitzen in der 
Regel eine bessere Qualität als die vorher vorgestellten Lösungen. Jedoch ist 
das Modellieren einer solchen Karte selbst für erfahrene Anwender aufwendig 
und langwierig, meist ist diese Methode nur für kleine Areale durchführbar. 
Hinzu kommt, dass der Modellierer meistens eine Vorlage für die Karte braucht, 
da er das genaue Aussehen der einzelnen Gebäude nicht auswendig kennt. 
Satellitenaufnahmen und vor Ort erstellte Bilder können als Vorlage benutzt 
werden. Besonders elegant ist es, mit einem \ac{3D}-Laserscanner die einzelnen 
Gebäude einzuscannen und mit den so gewonnen Daten die Karte zu erstellen 
\QuoteIndirectNoPage{Euclideon}.

\medskip
\autoref{fig:Machester} zeigt einen Ausschnitt aus einer solchen manuell 
modellierten \ac{3D}-Karte.

\begin{figure}[htb]
	%\Centerfloat
	\centering
	\includegraphics[scale=0.22]{Content/Implementation/Maps/Manchester.png}
	\caption{Ausschnitt aus einer manuell modellierten \ac{3D}-Karte}
	\label{fig:Machester}
\end{figure}