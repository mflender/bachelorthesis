# To make a .tex and .eps file texlive-eepic and texlive-bxeepic needs to be installed
set terminal epslatex color size 16cm, 9cm

# The name an location of the .tex and .eps file
set output "FourierOut.tex"

# The title of the graph
set title "\\sc Eine Sinus Kurve und deren Fouriersynthese"

# The lable for the x axsis
set xlabel "$x$-Achse"

# The lable and rotation for the y axsis
set ylabel "$y$-Achse" rotate by 0

# The position of the key
set key left top

# set the range from x=-Pi..Pi and y=-1..1
set xrange [-pi:pi]
set yrange [-1:1]

# Set the marking for x and y axsis to latex math mode (looks better)
set format y "$%g$"
set format x "$%.P ~\\pi$"

# Start the x axsis marking at -Pi and mark at a Pi  interval
set xtics -pi, pi

# Start the y axsis marking at -1 and mark every number
set ytics -1, 0.5

# The Fourier function
f(x,a)= sum [i=0:a] (1.0/(i*2+1)) * sin(x*((i*2+1)))

# Change the linetype (lt) to solid and the line color (lc) and the linewith (lw)
plot \
f(x,0) lt 1 lc 1 lw 2 title "$n=0$",\
f(x,2) lt 1 lc 2 lw 2 title "$n=2$",\
f(x,4) lt 1 lc 3 lw 2 title "$n=4$",\
f(x,6) lt 1 lc 4 lw 2 title "$n=6$"