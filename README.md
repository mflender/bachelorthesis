## Maltes Bachelorthesis

WS2015/2016, FH Bielefeld, Informatics

To compile this document you need to add the following switches to the makeindex: -g -s Index.ist %.idx

This thesis, a requirement for a bachelor's degree, deals with software to 
plan multicopter flights. At first, the field of application for such a 
multicopter and its technical and legal bounds is described, and the target 
group for such software is specified. The problems that could occur while using 
a multicopter will be solved by this software.

The functional and non-functional requirements of the problems will be derived, 
which will characterize the tool, which will be constructed. Conditions that 
come up with the use of a camera will also be discussed.

Afterwards the programs that are currently on the market and deal with this 
topic will be rated and presented. An evaluation of these applications based on 
the defined requirements will show us how far this problem has been already 
solved.

The implementation of the requirements will be executed by using a game engine. 
The reasons for this decision will be discussed as well. Afterwards, the 
requirements on such a game engine will be established. On the basis of these 
requirements the selection of the game engine used for this thesis will be 
explained. A market analysis describes the currently available game engines.

After a game engine has been chosen, the method of working with this engine 
will be described, and it will be substantiated why this method of working was 
chosen. Afterwards, the implementation of the software will be described. 
Lastly, the results will be compared to the requirements, and possibilities for 
further implementations will be mentioned.